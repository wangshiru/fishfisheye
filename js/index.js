
let svg = d3.select('#mysvg')

// 设定投影
let projection = d3.geoMercator().center([70.591796875, 55.494140625]).scale(1000).translate([0,0]).rotate([20])

// 获取中国经纬度
let chinaJson = getChinaJson()
console.log(chinaJson)
// 经纬度转 svg 二维坐标
let vector2ChinaJson = getVector2ChinaJson()
console.log(vector2ChinaJson)
// 绘制中国地图
drawChina()

// 鱼眼视图半径
let r = 150

// 鱼眼对象，distortion 为畸变系数
let fisheye = d3.fisheye.circular().radius(r).distortion(10)

// 当前焦点的鱼眼坐标
let f = null

// 防抖定时器
var timeObj = null

// 鱼眼坐标计算flag，false 表示当前未处于鱼眼视图，true 表示当前当前处于鱼眼视图
var changeFlag = false

// 给 svg 绑定鼠标移动事件
svg.on("mousemove", function(event){
    // 当前处于计算鱼眼视图
    if(changeFlag){
        // 获取所有已计算为鱼眼视图下的 line 对象 (class = isChange)，遍历所有对象，将每个 line ( x1, y1, x2, y2 ) 恢复初始坐标
        // 即获取中国初始地图
        d3.selectAll(".isChange")
            .call(function (d) {
                d._groups[0].forEach((line,j)=>{
                    let lineObj = d3.select(line)
                    lineObj.attr("class", "")
                        .transition() 
                        .duration(300)
                        .attr("x1", lineObj._groups[0][0].x1old)
                        .attr("y1", lineObj._groups[0][0].y1old)
                        .attr("x2", lineObj._groups[0][0].x2old)
                        .attr("y2", lineObj._groups[0][0].y2old)
                })
        })
    }
    // 如果防抖定时器存在则清除定时器
    if(timeObj){
        clearTimeout(timeObj)
    }
    // 设置防抖定时器， 200ms 后计算鱼眼视图的坐标
    timeObj = setTimeout(() => {
        updateLines(event)
    }, 300)
})

// 读取省地理区域json文件
function getChinaJson(){
    let chinaText = $.ajax({url:"/json/china.json",async:false})
    let chinaJson = decode(JSON.parse(chinaText.responseText))
    return chinaJson
}

// 将经纬度转化为 svg 二维坐标，并以 GeoJson 对象原格式输出
function getVector2ChinaJson(){
    let data = []
    chinaJson.features.forEach(areas => {
        var areasData = {
            properties: [],
            coordinates: []
        }
        areasData.properties = areas.properties
        areas.geometry.coordinates.forEach((points,i)=>{
            var arr = []
            points.forEach((point,j)=>{
                if(point[0] instanceof Array){
                    var a = []
                    point.forEach(p=>{
                        a.push(lnglatToMercator(p))
                    })
                    arr.push(a)
                }else{
                    arr.push(lnglatToMercator(point))
                }
            })
            areasData.coordinates.push(arr)
        })
        data.push(areasData)
    })
    return data
}

// 墨卡托转化
function lnglatToMercator(point){
    var p = []
    var point = [point[0],point[1]]
    var point =  projection(point)
    console.log()
    p.push(point[0],point[1])
    return p
}

// 绘制中国地图
function drawChina(){
    svg.append("g")
    for(let j in vector2ChinaJson){
        if(vector2ChinaJson[j].coordinates[0][0][0] instanceof Array){
            vector2ChinaJson[j].coordinates.forEach( points =>{
                for(let index = 0; index < points[0].length-1; index++){
                    svg.select("g")
                        .append("line")
                        .attr("x1", points[0][index][0])
                        .attr("y1", points[0][index][1])
                        .attr("x2", points[0][index+1][0])
                        .attr("y2", points[0][index+1][1])
                        .attr("stroke", "#000")
                        .attr("stroke-width", "2")
                        .attr("fill", "#000")
                }
            })
        }else{
            vector2ChinaJson[j].coordinates.forEach( points =>{
                for(let index = 0; index < points.length-1; index++){
                    svg.select("g")
                        .append("line")
                        .attr("x1", points[index][0])
                        .attr("y1", points[index][1])
                        .attr("x2", points[index+1][0])
                        .attr("y2", points[index+1][1])
                        .attr("stroke", "#000")
                        .attr("stroke-width", "2")
                        .attr("fill", "#000")
                }
            })
        }
    }
}

// 获取鱼眼视图内的 line 对象，并计算畸变后的坐标，再更新这些 line 对象的 ( x1, y1, x2, y2 )
function updateLines(event){
    // d3.pointer( event, this ) 返回值为当前鼠标焦点 [ x, y ] 位置
    let pointer = d3.pointer(event)
    // 鱼眼聚焦于当前鼠标焦点
    fisheye.focus(pointer)
    // 返回当前焦点的鱼眼视图坐标
    f = fisheye({x: pointer[0], y: pointer[1]})
    // 在 svg 中遍历所有 line 对象
    svg.selectAll("line")
        .call(function (d) {
            d._groups[0].forEach((line,j)=>{
                // 获取每个 line 对象的 ( x1, y1, x2, y2 )
                x1 = line.x1.animVal.value
                y1 = line.y1.animVal.value
                x2 = line.x2.animVal.value
                y2 = line.y2.animVal.value
                // 判断 line 对象的起点和终点是否有一个在鱼眼视图范围中，只要有一个在范围中就需要计算鱼眼畸变后的坐标并更新给 line 对象
                if(((Math.pow(x1-f.x, 2)+Math.pow(y1-f.y, 2))<Math.pow(r, 2))||((Math.pow(x2-f.x, 2)+Math.pow(y2-f.y, 2))<Math.pow(r, 2))){
                    // 获取 line 起点畸变后坐标
                    fisheye1 = fisheye({x: x1, y: y1})
                    // 获取 line 终点畸变后坐标
                    fisheye2 = fisheye({x: x2, y: y2})
                    // 获取 line 对象，保存原始坐标值，更新畸变坐标值，为处于鱼眼视图中的 line 对象设置 class = isChange
                    d3.select(line)
                        .property("x1old", x1)
                        .property("y1old", y1)
                        .property("x2old", x2)
                        .property("y2old", y2)
                        .transition() 
                        .duration(300)
                        .attr("x1", fisheye1.x)
                        .attr("y1", fisheye1.y)
                        .attr("x2", fisheye2.x)
                        .attr("y2", fisheye2.y)
                        .attr("class", "isChange")
                        // 当前视图为鱼眼视图，changeFlag 改为 true
                        changeFlag = true
                }
            })
        })
}
// 启动过渡效果
// .transition() 
// .duration(500)

// 计算网格
// let graticule = d3.geoGraticule()
// graticule.extent([[71,16],[137,54]]).step([2,2])
// let grid = graticule()

// console.log(grid)

// 设定路径生成器
// let path = d3.geoPath().projection(projection)

// 绘制中国
// svg.append("g")
//     .selectAll("path")
//     .data(chinaJson.features)
//     .enter()
//     .append("path")
//     .attr("class", "province")
//     .attr("d", path)


// 绘制网格
// svg.append("path")
    // .datum(grid)
    // .attr("class", "graticule")
    // .style("stroke", "#000")
    // .attr("d", path)

// 线段生成器
// let linePath = d3.line()

// svg.append("g")
//     .datum(vector2ChinaJson)
//     .each(function(d, i){
//         // 遍历数据集
//         for(let j in d){
//             // console.log(d[j].coordinates)
//             // console.log(d[j].coordinates[0][0][0] instanceof Array)
//             if(d[j].coordinates[0][0][0] instanceof Array){
//                 d[j].coordinates.forEach( points =>{
//                     // points.forEach( point =>{
//                     //     svg.select("g")
//                     //         .append("path")
//                     //         .attr("d", linePath(point))
//                     //         .attr("fill", "#000")
//                     //         .attr("stroke", "#fff")
//                     // })
//                     // console.log(points)
//                     // for(let k in points[0]){
//                     //     console.log(k)
//                     // }
//                     for(let index = 0; index < points[0].length-1; index++){
//                         svg.select("g")
//                             .append("line")
//                             .attr("x1", points[0][index][0])
//                             .attr("y1", points[0][index][1])
//                             .attr("x2", points[0][index+1][0])
//                             .attr("y2", points[0][index+1][1])
//                             .attr("stroke", "#000")
//                     }
//                 })
//             }else{
//                 d[j].coordinates.forEach( points =>{
//                     for(let index = 0; index < points.length-1; index++){
//                         svg.select("g")
//                             .append("line")
//                             .attr("x1", points[index][0])
//                             .attr("y1", points[index][1])
//                             .attr("x2", points[index+1][0])
//                             .attr("y2", points[index+1][1])
//                             .attr("stroke", "#000")
//                     }
//                     // svg.select("g")
//                     //     .append("path")
//                     //     .attr("d", linePath(points))
//                     //     .attr("fill", "#000")
//                     //     .attr("stroke", "#fff")
//                 })
//             }
//         }
//     })


/*
原理：
1.从地理坐标源文件中获取中国各个省市的边缘经纬度信息
2.根据墨卡托投影将中国各个省市的边缘经纬度信息（点坐标）转为屏幕坐标信息（点坐标）
3.根据省市的边缘位置的点坐标信息，将点连成线，在svg中添加 n 个 line对象，每个 line 对象包含( x1, y1, x2, y2 )属性
4.根据鼠标悬停焦点获取鱼眼相机（圆形）范围内的点坐标
5.遍历4获取的点，计算鱼眼畸变后的坐标
6.将畸变后的坐标幅值给line 对象的( x1, y1, x2, y2 )属性
7.鼠标移出时，将畸变的点坐标恢复为初始坐标

问题：
1.地理坐标源文件中点的数量非常多，几千个点坐标，在两个区域的重合边界线位置会绘制两次重合的边，导致svg中很多无用的line对象。此源文件目前没有更好的获取方式。
2.鱼眼视图的原理是更改坐标点位置，在更新 line 对象时可以获取坐标属性( x1, y1, x2, y2 )，更新即可。但如果是绘制区域不规则面积，不会像 line 对象可以直接获取点坐标位置。如果重绘不规则区域，内存代价也比较高，页面大概率会卡死。
3.如果不绘制区域，当前效果显然不满足以后的开发需求，效果比较简陋。
4.鱼眼视图适用于点，线，正方形，圆形等规则图形的查看，不太适用于像地图的不规则面积查看。
*/