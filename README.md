# mapFishEye

通过使用鱼眼视图，在大尺度下被弱化的细节信息被显著强化，使得可视分析用户可以在大尺度下同时兼顾细节信息。

# 工具

1. [d3.js](https://github.com/d3/d3)
2. [d3.geo.js](https://github.com/d3/d3-geo/tree/v3.0.1)
3. [d3-plugin fisheye.js](https://github.com/d3/d3-plugins)
4. china.json

## 获取中国地图轮廓经纬度

## 使用 d3.geo 在 svg 中绘制中国地图

### d3.geoPath 绘制中国地图

使用 d3.geoPath 直接处理 GeoJson 对象，在 svg 中追加 path。
鱼眼畸变计算是针对节点的，在生成的 path 中很难找到具体的形状对象，该方式虽然简单易生成矢量地图但不适合进行鱼眼视图的转换。

### svg line 绘制中国地图

通过解析 GeoJson 对象，获取各个点经纬度坐标，通过墨卡托投影转化成 svg 坐标，在 svg 中利用点连成线绘制 line。
在生成的 line 中可获取到起始位置 ( x1, y1, x2, y2 ) ，该方式虽然生成矢量地图较复杂但易于进行鱼眼视图的转换。

## 绑定鼠标滑动事件，更新鱼眼视图

# 注意事项

* 避免大量的遍历
* mouseover 和 mousemove 的区分